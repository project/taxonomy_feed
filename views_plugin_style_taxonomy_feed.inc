<?php

class views_plugin_style_taxonomy_feed extends views_plugin_style {
    /*
     * Set default options
     */

    function option_definition() {
        $options = parent::option_definition();
        return $options;
    }

    /**
     * Provide a form for setting options.
     *
     * @param array $form
     * @param array $form_state
     */
    function options_form(&$form, &$form_state) {
        $form['mission_description'] = array(
            '#type' => 'checkbox',
            '#default_value' => !empty($this->options['mission_description']),
            '#title' => t('Use the site mission for the description'),
        );
        $form['description'] = array(
            '#type' => 'textfield',
            '#title' => t('RSS description'),
            '#default_value' => $this->options['description'],
            '#description' => t('This will appear in the RSS feed itself.'),
            '#process' => array('views_process_dependency'),
            '#dependency' => array('edit-style-options-override' => array(FALSE)),
        );
    }

    function render() {
        if (empty($this->row_plugin)) {
            vpr('views_plugin_style_default: Missing row plugin');
            return;
        }
        $rows = '';

        // This will be filled in by the row plugin and is used later on in the
        // theming output.
        //$this->namespaces = array();
        // Fetch any additional elements for the channel and merge in their
        // namespaces.
        /* $this->channel_elements = $this->get_channel_elements();
          foreach ($this->channel_elements as $element) {
          if (isset($element['namespace'])) {
          $this->namespaces = array_merge($this->namespaces, $element['namespace']);
          }
          } */

        foreach ($this->view->result as $row) {
            $rows .= $this->row_plugin->render($row);
        }

        return theme($this->theme_functions(), $this->view, $this->options, $rows);
    }

}
