<?php

/**
 * Implementation of hook_views_plugings
 * @return <type>
 */

function taxonomy_feed_views_plugins()
{
  return array(
    'style' => array( //declare the taxonomy feed style plugin
      'taxonomy_feed' => array(
        'title' => t('Taxomony Feed'),
        'help' => t('Generates an RSS feed from a list of Taxonomy term.'),
        'handler' => 'views_plugin_style_taxonomy_feed',
        'theme' => 'views_view_taxonomy_feed',
        'uses row plugin' => TRUE,
        'uses fields' => FALSE,
        'uses options' => TRUE,
        'type' => 'feed',
        'help topic' => 'style-rss',
      ),
    ),
    'row' => array( //declare the rss row plugin for terms
      'taxonomy_term' => array(
        'title' => t('Taxomony Term'),
        'help' => t('(Displays the term data for each row from the views query with each row on a new line.'),
        'handler' => 'views_plugin_row_taxonomy_term',
        'theme' => 'views_view_row_taxonony_term',
        'uses fields' => FALSE,
        'uses options' => FALSE,
        'type' => 'feed',
        'help topic' => 'style-rss',
      ),
     )
  );
}