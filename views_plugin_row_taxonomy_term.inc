<?php

// $Id
/**
 * @file
 * Contains the base taxonomy term row style plugin.
 */

class views_plugin_row_taxonomy_term extends views_plugin_row {

    // Basic properties that let the row style follow relationships.
    var $base_table = 'term_data';
    var $base_field = 'tid';

    function render($row) {

        if (!is_numeric($row->tid)) {
            return;
        }

        $term = taxonomy_get_term($row->tid);
        $item = new stdClass();
        $item->title = $term->name;
        $item->link = url('taxonomy/term/' . $term->tid . '/0/feed', array('absolute' => TRUE));

        $item->description = $term->description;


        return theme($this->theme_functions(), $this->view, $this->options, $item);
    }

}

